# customerservice


| Method  |   Path    			                                                      | 		 Description 				|
| :---           |     :---:     		                                                          |          ---: 					|
|POST		| /customer			                            	                  |	Create Customer         	    |
|GET		    | /customer/sim/{customer_id}	            		|	Retrieve Customer Sims			| 
|POST		|/customer/link-sim/{customer_id}/{sim_id}	|	Link sim to Customer 			|
|POST		| /sim 					                                                  |	Create Sim 						|
|GET		    | /sim/all				                                                  |	Retrieve all Sims               |


How to run:- <br /> 
1. checkout project using command - git clone https://07ROhit@bitbucket.org/07ROhit/customerservice.git <br /> 
2. Change database config in application.yml <br /> 
3. Change csv location in applicaton.yml <br /> 
4. Run initial_setup.sql <br /> 


# scheduled-jobs
1. birthday-notification -> Send email to customer wishing advance happy birthday <br/>
2. Customer-export -> send csv file having data of all customers having their birthday on the day <br/> 
3. Cron job started every day at 2.00 pm. which can modifed from application.yml
