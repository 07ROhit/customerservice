package com.org.haud.customerservice.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.org.haud.customerservice.CustomerserviceApplication;
import com.org.haud.customerservice.dto.SimCardDto;
import com.org.haud.customerservice.service.SimCardService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.MOCK, classes = { CustomerserviceApplication.class })
public class SimControllerTest {
	
	private MockMvc mockMvc;

	@MockBean
	private SimCardService  simService;

	@Autowired
	private WebApplicationContext webApplicationContext;

	private SimCardDto simCarddto;
	
	@BeforeEach
	public void setUp() {
		simCarddto = SimCardDto.builder().Id(3L).ICCID("asd123").IMSI("1234567890").build();
		this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}
	
	@Test
	void testCreateSim() throws Exception {
		//ResponseDto build2 = ResponseDto.builder().data(entity).build();
		when(simService.createSimCard(simCarddto)).thenReturn(simCarddto);

		mockMvc.perform(post("/sim")
				.contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(simCarddto))
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
	        	.andDo(print());
	}
	
	public static String asJsonString(final Object obj) {
	    try {
	        return new ObjectMapper().writeValueAsString(obj);
	    } catch (Exception e) {
	        throw new RuntimeException(e);
	    }
	}
}
