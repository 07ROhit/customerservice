package com.org.haud.customerservice.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.org.haud.customerservice.CustomerserviceApplication;
import com.org.haud.customerservice.dto.CustomerDto;
import com.org.haud.customerservice.dto.SimCardDto;
import com.org.haud.customerservice.entity.SimCard;
import com.org.haud.customerservice.service.CustomerService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.MOCK, classes = { CustomerserviceApplication.class })
public class CustomerControllerTest {

	private MockMvc mockMvc;

	@MockBean
	private CustomerService custservice;

	@Autowired
	private WebApplicationContext webApplicationContext;

	private CustomerDto dto;
	private Set<SimCard> simList = new HashSet<>();
	private SimCard sim;
	
	@BeforeEach
	public void setUp() {
		dto = CustomerDto.builder().id(5L).firstName("Rohit").lastName("Sarwade").email("srNs@gmail.com")
				.dateOfBirth(new Date()).build();
		sim = SimCard.builder().Id(3L).ICCID("asd123").IMSI("1234567890").build();
		simList.add(sim);
		this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}

	@Test
	void testCreateUser() throws Exception {
		//ResponseDto build2 = ResponseDto.builder().data(entity).build();
		when(custservice.createUser(dto)).thenReturn(dto);

		mockMvc.perform(post("/customer")
				.contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(dto))
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
	        	.andDo(print());
	}

	@Test
	void testFindAllSimsByCustomerId() throws Exception {
		Set<SimCardDto> simDtoList = new HashSet<>();
		simList.stream().forEach(sim -> {
			SimCardDto simDto = SimCardDto.builder().Id(sim.getId()).ICCID(sim.getICCID()).IMSI(sim.getIMSI()).build();
			simDto.setCustId(dto.getId());
			simDtoList.add(simDto);
		});
		when(custservice.findAllSimsByCustomerId(5l)).thenReturn(simDtoList);

		mockMvc.perform(get("/customer/sim/{customer_id}", 5l)
				.accept(MediaType.APPLICATION_JSON))
				.andDo(print())
		      .andExpect(status().isOk());
	}
	
	@Test
	void testCustNotFoundFindAllSimsByCustomerId() throws Exception {
		Set<SimCardDto> simDtoList = new HashSet<>();
		simList.stream().forEach(sim -> {
			SimCardDto simDto = SimCardDto.builder().Id(sim.getId()).ICCID(sim.getICCID()).IMSI(sim.getIMSI()).build();
			simDto.setCustId(dto.getId());
			simDtoList.add(simDto);
		});
		when(custservice.findAllSimsByCustomerId(dto.getId())).thenReturn(simDtoList);

		mockMvc.perform(get("/customer/sim/{customer_id}", 9l)
				.accept(MediaType.APPLICATION_JSON))
				.andDo(print())
		       .andExpect(status().isNotFound());
				
	}

	@Test
	void testLinkCustomerAndSim() throws Exception {
		when(custservice.linkSim(dto.getId(),sim.getId())).thenReturn(true);
		
		mockMvc.perform(post("/customer/link-sim/{customer_id}/{sim_id}", 5L,3L)
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
	        	.andDo(print());
	}
	
	@Test
	void testNotFoundLinkCustomerAndSim() throws Exception {
		when(custservice.linkSim(dto.getId(),sim.getId())).thenReturn(true);
		
		mockMvc.perform(post("/customer/link-sim/{customer_id}/{sim_id}", 5L,4L)
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isNotFound())
	        	.andDo(print());
	}
	
	
	public static String asJsonString(final Object obj) {
	    try {
	        return new ObjectMapper().writeValueAsString(obj);
	    } catch (Exception e) {
	        throw new RuntimeException(e);
	    }
	}
}
