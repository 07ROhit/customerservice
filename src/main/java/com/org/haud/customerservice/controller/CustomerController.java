package com.org.haud.customerservice.controller;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.org.haud.customerservice.dto.CustomerDto;
import com.org.haud.customerservice.dto.ResponseDto;
import com.org.haud.customerservice.dto.SimCardDto;
import com.org.haud.customerservice.exception.CustomerServiceException;
import com.org.haud.customerservice.service.CustomerService;

@RestController
@RequestMapping(value = "/customer")
public class CustomerController {

	@Autowired
	private CustomerService customerService;

	@PostMapping
	public ResponseEntity<CustomerDto> createCustomer(@RequestBody CustomerDto customerDto)
			throws CustomerServiceException {
		CustomerDto responseDto = customerService.createUser(customerDto);
		return ResponseEntity.ok(responseDto);
	}

	@GetMapping("/sim/{customer_id}")
	public ResponseEntity<ResponseDto> retrieveCustomerSims(@PathVariable(name = "customer_id") Long customerId) {
		Set<SimCardDto> findAllSimsByCustomerId = customerService.findAllSimsByCustomerId(customerId);
		if (findAllSimsByCustomerId.isEmpty()) {
			ResponseDto build = ResponseDto.builder().errors("Customer not Found").build();
			return new ResponseEntity<>(build, HttpStatus.NOT_FOUND);
		}
		ResponseDto build = ResponseDto.builder().data(findAllSimsByCustomerId).build();
		return new ResponseEntity<>(build, HttpStatus.OK);
	}

	@PostMapping("/link-sim/{customer_id}/{sim_id}")
	public ResponseEntity<ResponseDto> linkCustomerAndSim(@PathVariable(name = "customer_id") Long customerId,
			@PathVariable(name = "sim_id") Long simId) throws CustomerServiceException {
		Boolean linkSim = customerService.linkSim(customerId, simId);
		if (linkSim) {
			return ResponseEntity.ok(ResponseDto.builder().build());
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

}
